-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 17, 2018 at 02:25 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finder`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `closing_date` datetime DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `salary` double DEFAULT NULL,
  `min_rate` double DEFAULT NULL,
  `max_rate` double DEFAULT NULL,
  `fixed_rate` double DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `marked` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `title`, `category_id`, `city_id`, `closing_date`, `type_id`, `description`, `salary`, `min_rate`, `max_rate`, `fixed_rate`, `hours`, `marked`, `created_at`, `updated_at`) VALUES
(1, 'Restaurant Team Member - Crew', 4, 11, '2018-12-31 00:00:00', 1, '\"<p class=\\\"margin-reset\\\" style=\\\"padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102); margin-bottom: 0px !important;\\\">The Food Service Specialist ensures outstanding customer service is provided to food customers and that all food offerings meet the required stock levels and presentation standards. Beginning your career as a Food Steward will give you a strong foundation in Speedway\\u2019s food segment that can make you a vital member of the front line team!<\\/p><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><p style=\\\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102);\\\">The&nbsp;<span style=\\\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; color: rgb(51, 51, 51);\\\">Food Service Specialist<\\/span>&nbsp;will have responsibilities that include:<\\/p><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Executing the Food Service program, including preparing and presenting our wonderful food offerings to hungry customers on the go!<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Keeping our Store food area looking terrific and ready for our valued customers by managing product inventory, stocking, cleaning, etc.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">We\\u2019re looking for associates who enjoy interacting with people and working in a fast-paced environment!<\\/li><\\/ul><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><h4 class=\\\"margin-bottom-10\\\" style=\\\"margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 36px; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(51, 51, 51);\\\">Job Requirment<\\/h4><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be available to work required shifts including weekends, evenings and holidays.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to perform repeated bending, standing and reaching.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to occasionally lift up to 50 pounds<\\/li><\\/ul>\"', NULL, NULL, NULL, NULL, NULL, 0, '2018-11-14 16:22:09', '2018-11-30 18:43:57'),
(2, 'Restaurant Team Member - Crew', 4, 11, '2018-12-31 00:00:00', 2, '\"<p class=\\\"margin-reset\\\" style=\\\"padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102); margin-bottom: 0px !important;\\\">The Food Service Specialist ensures outstanding customer service is provided to food customers and that all food offerings meet the required stock levels and presentation standards. Beginning your career as a Food Steward will give you a strong foundation in Speedway\\u2019s food segment that can make you a vital member of the front line team!<\\/p><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><p style=\\\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102);\\\">The&nbsp;<span style=\\\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; color: rgb(51, 51, 51);\\\">Food Service Specialist<\\/span>&nbsp;will have responsibilities that include:<\\/p><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Executing the Food Service program, including preparing and presenting our wonderful food offerings to hungry customers on the go!<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Keeping our Store food area looking terrific and ready for our valued customers by managing product inventory, stocking, cleaning, etc.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">We\\u2019re looking for associates who enjoy interacting with people and working in a fast-paced environment!<\\/li><\\/ul><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><h4 class=\\\"margin-bottom-10\\\" style=\\\"font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 36px; color: rgb(51, 51, 51); margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline;\\\">Job Requirment<\\/h4><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be available to work required shifts including weekends, evenings and holidays.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to perform repeated bending, standing and reaching.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to occasionally lift up to 50 pounds<\\/li><\\/ul>\"', NULL, NULL, NULL, NULL, NULL, 0, '2018-11-29 14:57:37', '2018-11-29 14:57:37'),
(3, 'Restaurant Team Member - Crew', 4, 11, '2018-12-31 00:00:00', 3, '\"<p class=\\\"margin-reset\\\" style=\\\"padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102); margin-bottom: 0px !important;\\\">The Food Service Specialist ensures outstanding customer service is provided to food customers and that all food offerings meet the required stock levels and presentation standards. Beginning your career as a Food Steward will give you a strong foundation in Speedway\\u2019s food segment that can make you a vital member of the front line team!<\\/p><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><p style=\\\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102);\\\">The&nbsp;<span style=\\\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; color: rgb(51, 51, 51);\\\">Food Service Specialist<\\/span>&nbsp;will have responsibilities that include:<\\/p><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Executing the Food Service program, including preparing and presenting our wonderful food offerings to hungry customers on the go!<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Keeping our Store food area looking terrific and ready for our valued customers by managing product inventory, stocking, cleaning, etc.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">We\\u2019re looking for associates who enjoy interacting with people and working in a fast-paced environment!<\\/li><\\/ul><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><h4 class=\\\"margin-bottom-10\\\" style=\\\"font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 36px; color: rgb(51, 51, 51); margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline;\\\">Job Requirment<\\/h4><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be available to work required shifts including weekends, evenings and holidays.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to perform repeated bending, standing and reaching.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to occasionally lift up to 50 pounds<\\/li><\\/ul>\"', NULL, NULL, NULL, NULL, NULL, 0, '2018-11-29 15:07:25', '2018-11-29 15:07:25'),
(4, 'Restaurant Team Member - Crew', 4, 11, '2018-12-11 00:00:00', 4, '\"<p class=\\\"margin-reset\\\" style=\\\"padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102); margin-bottom: 0px !important;\\\">The Food Service Specialist ensures outstanding customer service is provided to food customers and that all food offerings meet the required stock levels and presentation standards. Beginning your career as a Food Steward will give you a strong foundation in Speedway\\u2019s food segment that can make you a vital member of the front line team!<\\/p><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><p style=\\\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102);\\\">The&nbsp;<span style=\\\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; color: rgb(51, 51, 51);\\\">Food Service Specialist<\\/span>&nbsp;will have responsibilities that include:<\\/p><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Executing the Food Service program, including preparing and presenting our wonderful food offerings to hungry customers on the go!<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Keeping our Store food area looking terrific and ready for our valued customers by managing product inventory, stocking, cleaning, etc.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">We\\u2019re looking for associates who enjoy interacting with people and working in a fast-paced environment!<\\/li><\\/ul><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><h4 class=\\\"margin-bottom-10\\\" style=\\\"font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 36px; color: rgb(51, 51, 51); margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline;\\\">Job Requirment<\\/h4><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be available to work required shifts including weekends, evenings and holidays.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to perform repeated bending, standing and reaching.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to occasionally lift up to 50 pounds<\\/li><\\/ul>\"', 1, 2500, 2500, 2500, 40, 1, '2018-11-29 15:08:17', '2018-11-30 19:23:18'),
(5, 'Restaurant Team Member - Crew', 4, 11, '2018-12-31 00:00:00', 4, '\"<p class=\\\"margin-reset\\\" style=\\\"padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102); margin-bottom: 0px !important;\\\">The Food Service Specialist ensures outstanding customer service is provided to food customers and that all food offerings meet the required stock levels and presentation standards. Beginning your career as a Food Steward will give you a strong foundation in Speedway\\u2019s food segment that can make you a vital member of the front line team!<\\/p><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><p style=\\\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(102, 102, 102);\\\">The&nbsp;<span style=\\\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; color: rgb(51, 51, 51);\\\">Food Service Specialist<\\/span>&nbsp;will have responsibilities that include:<\\/p><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Executing the Food Service program, including preparing and presenting our wonderful food offerings to hungry customers on the go!<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Keeping our Store food area looking terrific and ready for our valued customers by managing product inventory, stocking, cleaning, etc.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">We\\u2019re looking for associates who enjoy interacting with people and working in a fast-paced environment!<\\/li><\\/ul><p><br style=\\\"color: rgb(102, 102, 102); font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\"><\\/p><h4 class=\\\"margin-bottom-10\\\" style=\\\"font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 36px; color: rgb(51, 51, 51); margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline;\\\">Job Requirment<\\/h4><ul class=\\\"list-1\\\" style=\\\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: inherit; font-family: Montserrat, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; vertical-align: baseline; list-style: none outside; color: rgb(102, 102, 102);\\\"><li style=\\\"margin: 0px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be available to work required shifts including weekends, evenings and holidays.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to perform repeated bending, standing and reaching.<\\/li><li style=\\\"margin: 9px 0px 9px 20px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 24px; font-family: inherit; vertical-align: baseline; list-style: none; position: relative;\\\">Must be able to occasionally lift up to 50 pounds<\\/li><\\/ul>\"', 2, 501, 850, 0, 40, 1, '2018-11-29 15:10:50', '2018-11-30 19:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `ads_keyword`
--

DROP TABLE IF EXISTS `ads_keyword`;
CREATE TABLE IF NOT EXISTS `ads_keyword` (
  `job_id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads_keyword`
--

INSERT INTO `ads_keyword` (`job_id`, `keyword_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-11-14 16:23:31', '2018-11-14 16:23:31'),
(2, 1, '2018-11-29 14:57:38', '2018-11-29 14:57:38'),
(3, 1, '2018-11-29 15:07:25', '2018-11-29 15:07:25'),
(4, 1, '2018-11-29 15:08:17', '2018-11-29 15:08:17'),
(5, 1, '2018-11-29 15:10:50', '2018-11-29 15:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `all_slug`
--

DROP TABLE IF EXISTS `all_slug`;
CREATE TABLE IF NOT EXISTS `all_slug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_slug`
--

INSERT INTO `all_slug` (`id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'it', NULL, NULL),
(2, 'php', NULL, NULL),
(3, 'accounts', NULL, NULL),
(4, 'banking', '2018-06-05 11:29:53', '2018-06-05 11:29:53'),
(5, 'java', '2018-06-05 11:31:48', '2018-06-05 11:31:48'),
(6, 'software-engineer', '2018-06-05 11:56:11', '2018-06-05 11:56:11'),
(7, 'full-time', '2018-11-14 13:13:19', '2018-11-14 13:13:19'),
(8, 'part-time', '2018-11-14 13:14:16', '2018-11-14 13:14:16'),
(9, 'internship', '2018-11-14 13:14:32', '2018-11-14 13:14:32'),
(10, 'freelance', '2018-11-14 13:14:43', '2018-11-14 13:14:43'),
(11, 'western-province', '2018-11-14 13:33:29', '2018-11-14 13:33:29'),
(12, 'southern-province', '2018-11-14 13:34:07', '2018-11-14 13:34:07'),
(13, 'central-province', '2018-11-14 13:34:40', '2018-11-14 13:34:40'),
(14, 'eastern-province', '2018-11-14 13:34:53', '2018-11-14 13:34:53'),
(15, 'north-central-province', '2018-11-14 13:35:12', '2018-11-14 13:35:12'),
(16, 'northern-province', '2018-11-14 13:35:19', '2018-11-14 13:35:19'),
(17, 'north-western-province', '2018-11-14 13:35:33', '2018-11-14 13:35:33'),
(18, 'sabaragamuwa-province', '2018-11-14 13:35:59', '2018-11-14 13:35:59'),
(19, 'uva-province', '2018-11-14 13:36:24', '2018-11-14 13:36:24'),
(22, 'colombo', '2018-11-14 16:16:50', '2018-11-14 16:16:50'),
(21, 'colombo-district', '2018-11-14 13:44:20', '2018-11-14 13:44:20'),
(23, 'restaurant-food-service', '2018-11-14 16:20:15', '2018-11-14 16:20:15'),
(24, 'restaurant-team-member', '2018-11-29 15:34:44', '2018-11-29 15:34:44'),
(25, 'crew', '2018-11-29 15:34:51', '2018-11-29 15:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `applied_jobs`
--

DROP TABLE IF EXISTS `applied_jobs`;
CREATE TABLE IF NOT EXISTS `applied_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `cover_letter` text NOT NULL,
  `cv_path` text NOT NULL,
  `marked` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `icon`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'IT', 'it', NULL, 1, 2, '2018-05-27 11:03:53', '2018-05-28 16:48:45'),
(2, 'Accounts', 'accounts', NULL, 1, 2, '2018-05-28 12:13:14', '2018-05-28 17:46:04'),
(3, 'Banking', 'banking', NULL, 1, 2, '2018-06-05 11:29:53', '2018-06-05 11:29:53'),
(4, 'Restaurant / Food Service', 'restaurant-food-service', 'ln-icon-Fish', 1, 2, '2018-11-14 16:20:15', '2018-11-14 16:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo_path` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `logo_path`, `created_at`, `updated_at`) VALUES
(1, 'logo/logo-ex-7.png', '2018-11-18 17:02:54', '2018-11-18 17:05:01'),
(2, 'logo/logo-ex-7.png', '2018-11-18 17:25:22', '2018-11-18 17:25:22');

-- --------------------------------------------------------

--
-- Table structure for table `job_type`
--

DROP TABLE IF EXISTS `job_type`;
CREATE TABLE IF NOT EXISTS `job_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_type`
--

INSERT INTO `job_type` (`id`, `name`, `slug`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Full-Time', 'full-time', 1, 2, '2018-11-14 13:13:19', '2018-11-14 13:13:19'),
(2, 'Part-Time', 'part-time', 1, 2, '2018-11-14 13:14:16', '2018-11-14 13:14:16'),
(3, 'Internship', 'internship', 1, 2, '2018-11-14 13:14:32', '2018-11-14 13:14:32'),
(4, 'Freelance', 'freelance', 1, 2, '2018-11-14 13:14:43', '2018-11-14 13:14:43');

-- --------------------------------------------------------

--
-- Table structure for table `keyword`
--

DROP TABLE IF EXISTS `keyword`;
CREATE TABLE IF NOT EXISTS `keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keyword`
--

INSERT INTO `keyword` (`id`, `name`, `slug`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Software Engineer', 'software-engineer', 1, 2, '2018-06-05 11:56:11', '2018-06-05 11:56:11'),
(2, 'Restaurant Team Member', 'restaurant-team-member', 1, 2, '2018-11-29 15:34:44', '2018-11-29 15:34:44'),
(3, 'Crew', 'crew', 1, 2, '2018-11-29 15:34:51', '2018-11-29 15:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `province_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `slug`, `province_id`, `district_id`, `type`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Western Province', 'western-province', 0, 0, 0, 1, 2, '2018-11-14 13:33:29', '2018-11-14 13:33:29'),
(2, 'Southern Province', 'southern-province', 0, 0, 0, 1, 2, '2018-11-14 13:34:07', '2018-11-14 13:34:07'),
(3, 'Central Province', 'central-province', 0, 0, 0, 1, 2, '2018-11-14 13:34:40', '2018-11-14 13:34:40'),
(4, 'Eastern Province', 'eastern-province', 0, 0, 0, 1, 2, '2018-11-14 13:34:53', '2018-11-14 13:34:53'),
(5, 'North Central Province', 'north-central-province', 0, 0, 0, 1, 2, '2018-11-14 13:35:12', '2018-11-14 13:35:12'),
(6, 'Northern Province', 'northern-province', 0, 0, 0, 1, 2, '2018-11-14 13:35:19', '2018-11-14 13:35:19'),
(7, 'North Western Province', 'north-western-province', 0, 0, 0, 1, 2, '2018-11-14 13:35:33', '2018-11-14 13:35:33'),
(8, 'Sabaragamuwa Province', 'sabaragamuwa-province', 0, 0, 0, 1, 2, '2018-11-14 13:35:59', '2018-11-14 13:35:59'),
(9, 'Uva Province', 'uva-province', 0, 0, 0, 1, 2, '2018-11-14 13:36:24', '2018-11-14 13:36:24'),
(10, 'Colombo District', 'colombo-district', 1, 0, 1, 1, 2, '2018-11-14 13:37:21', '2018-11-14 13:44:45'),
(11, 'Colombo', 'colombo', 1, 10, 2, 1, 2, '2018-11-14 16:16:50', '2018-11-14 16:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `send_emails`
--

DROP TABLE IF EXISTS `send_emails`;
CREATE TABLE IF NOT EXISTS `send_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailTo` text NOT NULL,
  `message` text NOT NULL,
  `template` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `name`, `slug`, `status`, `category_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'PHP', 'php', 1, 1, 2, '2018-05-27 12:47:21', '2018-05-28 11:40:07'),
(2, 'Java', 'java', 1, 1, 2, '2018-06-05 11:31:48', '2018-06-05 11:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) NOT NULL,
  `client_note` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `client_name`, `client_note`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Collis Ta’eed, Envato', '\"I have already heard back about the internship I applied through Job Finder, that\'s the fastest job reply I\'ve ever gotten and it\'s so much better than waiting weeks to hear back. test\"', 1, '2018-11-18 16:03:32', '2018-11-18 16:20:14'),
(3, 'DN', '\"test testimonial section\"', 1, '2018-11-18 16:21:05', '2018-11-18 16:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `user_type`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Danuja', 'ldashanfernando@gmail.com', '$2y$10$XB/3PhdFqOQAExtxXib4Bufj0wdDNT.rgPF/J0UnDmOZVISy7vRuy', 'glsRjUQXE0Zcchha9wRtadw0qmbmvYlCbcCsXduIkrWpzwJ6hDcmz1CcsLq0', 1, 1, '2018-05-22 03:23:39', '2018-11-11 13:56:44'),
(3, 'Ashan', 'ashan@gmail.com', '$2y$10$g6x/KKsGWFbo.EPMN7LdNe42IGEleJG/PD72SFtwlKRUwSkt7qMGS', NULL, 2, 1, '2018-05-27 09:22:54', '2018-05-27 09:31:29'),
(4, 'Developer', 'developer@gmail.com', '$2y$10$Sl8ptaysokGQK01Npcd3UuBnDMnpnL13Q2OqUq2qaibMgd3183FEi', 'y7zadvy9ITse0a856TQe60y9yOUM1wJ9N2OkuPnuLE5vR4dsJj094GaX79t3', 1, 1, '2018-06-06 11:10:26', '2018-06-06 11:10:26');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
