@extends('layouts.default')

@section('content')
    <h1 class="page-title">Sub category</h1>
    <div class="row">
        <div class="col-md-6 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Sub category Edit ID :: {{ $sub_category->id }}</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('sub-category.edit.post',[$sub_category->id]) }}" method="post" role="form" class="sub-category-create-form">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Name</label>
                                    <div class="input-group">
                                        <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }} input-icon" placeholder="Name" value="{{ $sub_category->name }}">
                                        <span class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Slug</label>
                                    <div class="input-group">
                                        <input type="text" name="slug" id="slug" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }} input-icon" placeholder="Slug" value="{{ $sub_category->slug }}">
                                        <span class="input-group-addon">
                                             <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </span>
                                @if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <span class="form-error">
                                    <label>Category</label>
                                     <select name="category" class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}">
                                        <option value="" selected="true" disabled="true">Select Category</option>
                                         @foreach($category as $ct)
                                             <option value="{{ $ct->id }}" @if($ct->id == $sub_category->category_id) selected="true" @endif>{{ $ct->name }}</option>
                                         @endforeach
                                    </select>
                                </span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" id="hidden_slug" value="{{ $sub_category->slug }}">
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="{{ route('sub-category.list.get') }}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#slug').focusout(function ($e) {
                var keyword = $(this).val();
                var hidden_slug = $('#hidden_slug').val();
                if(keyword != hidden_slug)
                {
                    slugCreate(keyword);
                }
            });
        });
        function slugCreate(keyword){
            var _token = $('input[name="_token"]').val();
            $('.swing-preloader').css('display','block');
            $.ajax({
                url: '{{ route('generate-slug') }}',
                type: "post",
                data: {_token: _token, keyword:keyword},
                success: function (data) {
                    console.log(data);
                    $('#slug').val(data);
                    $('.swing-preloader').css('display','none');
                    $('#slug').closest('.form-group').removeClass('has-error');
                    $('#slug-error').remove();
                }
            });
        }
    </script>
@endsection