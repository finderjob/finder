@extends('layouts.default')

@section('content')
    <h1 class="page-title">Sub Category</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Sub Category List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('sub-category.add.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Sub Category Id</th>
                                        <th> Name </th>
                                        <th> Slug </th>
                                        <th> Category </th>
                                        <th> Status </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sub_category_list as $scl)
                                    <tr>
                                        <td>{{ $scl->id }}</td>
                                        <td>{{ $scl->name }}</td>
                                        <td>{{ $scl->slug }}</td>
                                        <?php $category = \App\Category::find($scl->category_id);
                                                if($category)
                                                {
                                                    $category = $category->name;
                                                }
                                        ?>
                                        <td>{{ $category }}</td>
                                        <td>
                                            @if($scl->status)
                                                <span class="label label-sm label-success"> Activate </span>
                                            @else
                                                <span class="label label-sm label-danger"> Deactivate </span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="{{ route('sub-category.edit.get',[$scl->id]) }}" title="edit">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            @if($scl->status)
                                                <a class="btn btn-circle btn-icon-only btn-danger" title="Deactivate" href="{{ route('sub-category.deactivate',$scl->id) }}"onclick="event.preventDefault();
                                                        document.getElementById('sub-category-deactivate-{{ $scl->id }}').submit();">
                                                    <i class="fa fa-minus"></i>
                                                </a>
                                                <form id="sub-category-deactivate-{{ $scl->id }}" action="{{ route('sub-category.deactivate',$scl->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            @else
                                                <a class="btn btn-circle btn-icon-only btn-success" title="Activate" href="{{ route('sub-category.activate',$scl->id) }}" onclick="event.preventDefault();
                                                        document.getElementById('sub-category-activate-{{ $scl->id }}').submit();">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <form id="sub-category-activate-{{ $scl->id }}" action="{{ route('sub-category.activate',$scl->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
@endsection