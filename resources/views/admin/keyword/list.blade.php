@extends('layouts.default')

@section('content')
    <h1 class="page-title">Keywords</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Keywords List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('keywords.add.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Keyword Id</th>
                                        <th> Name </th>
                                        <th> Slug </th>
                                        <th> Status </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($keyword_list as $kl)
                                    <tr>
                                        <td>{{ $kl->id }}</td>
                                        <td>{{ $kl->name }}</td>
                                        <td>{{ $kl->slug }}</td>
                                        <td>
                                            @if($kl->status)
                                                <span class="label label-sm label-success"> Activate </span>
                                            @else
                                                <span class="label label-sm label-danger"> Deactivate </span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="{{ route('keywords.edit.get',[$kl->id]) }}" title="edit">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            @if($kl->status)
                                                <a class="btn btn-circle btn-icon-only btn-danger" title="Deactivate" href="{{ route('keywords.deactivate',$kl->id) }}"onclick="event.preventDefault();
                                                        document.getElementById('keywords-deactivate-{{ $kl->id }}').submit();">
                                                    <i class="fa fa-minus"></i>
                                                </a>
                                                <form id="keywords-deactivate-{{ $kl->id }}" action="{{ route('keywords.deactivate',$kl->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            @else
                                                <a class="btn btn-circle btn-icon-only btn-success" title="Activate" href="{{ route('keywords.activate',$kl->id) }}" onclick="event.preventDefault();
                                                        document.getElementById('keywords-activate-{{ $kl->id }}').submit();">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <form id="keywords-activate-{{ $kl->id }}" action="{{ route('keywords.activate',$kl->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
@endsection