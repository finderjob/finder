@extends('layouts.default')

@section('content')
    <h1 class="page-title">Category</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Category List</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ route('category.add.get') }}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="user_list_table" role="grid" >
                                <thead>
                                    <tr role="row">
                                        <th>Category Id</th>
                                        <th> Name </th>
                                        <th> Slug</th>
                                        <th> Status </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($category_list as $cl)
                                    <tr>
                                        <td>{{ $cl->id }}</td>
                                        <td>{{ $cl->name }}</td>
                                        <td>{{ $cl->slug }}</td>
                                        <td>
                                            @if($cl->status)
                                                <span class="label label-sm label-success"> Activate </span>
                                            @else
                                                <span class="label label-sm label-danger"> Deactivate </span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="{{ route('category.edit.get',[$cl->id]) }}" title="edit">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            @if($cl->status)
                                                <a class="btn btn-circle btn-icon-only btn-danger" title="Deactivate" href="{{ route('category.deactivate',$cl->id) }}"onclick="event.preventDefault();
                                                        document.getElementById('category-deactivate-{{ $cl->id }}').submit();">
                                                    <i class="fa fa-minus"></i>
                                                </a>
                                                <form id="category-deactivate-{{ $cl->id }}" action="{{ route('category.deactivate',$cl->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            @else
                                                <a class="btn btn-circle btn-icon-only btn-success" title="Activate" href="{{ route('category.activate',$cl->id) }}" onclick="event.preventDefault();
                                                        document.getElementById('category-activate-{{ $cl->id }}').submit();">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <form id="category-activate-{{ $cl->id }}" action="{{ route('category.activate',$cl->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
@endsection