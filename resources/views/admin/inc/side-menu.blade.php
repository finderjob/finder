<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse" aria-expanded="false" style="height: 0px;">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <?php $route_name = \Request::route()->getName(); ?>

            <li class="nav-item <?php echo ($route_name == 'admin.home' ) ? "start active open" : ""?>">
                <a href="{{ route('admin.home') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <?php echo ($route_name == 'admin.home' ) ? "<span class='selected'></span>" : ""?>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">User Management</h3>
            </li>
            <?php  $user_routes_names = array('admin.list','admin.users.add');?>
            <li class="nav-item <?php echo (in_array($route_name, $user_routes_names)) ? "start active open" : ""?>">
                <a href="{{ route('admin.list') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Users</span>
                    <?php echo (in_array($route_name, $user_routes_names)) ? "<span class='selected'></span>" : ""?>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Permission</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Customer Management</h3>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Customers</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Ads Management</h3>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Ads</span>
                </a>
            </li>
            <?php  $category_routes_names = array('category.list.get','category.add.get','category.edit.get');?>
            <li class="nav-item <?php echo (in_array($route_name, $category_routes_names)) ? "start active open" : ""?>">
                <a href="{{ route('category.list.get') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Category</span>
                    <?php echo (in_array($route_name, $category_routes_names)) ? "<span class='selected'></span>" : ""?>
                </a>
            </li>
            <?php  $sub_category_routes_names = array('sub-category.list.get','sub-category.add.get','sub-category.edit.get');?>
            <li class="nav-item <?php echo (in_array($route_name, $sub_category_routes_names)) ? "start active open" : ""?>">
                <a href="{{ route('sub-category.list.get') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Sub Category</span>
                    <?php echo (in_array($route_name, $sub_category_routes_names)) ? "<span class='selected'></span>" : ""?>
                </a>
            </li>
            <?php  $keyword_routes_names = array('keywords.list.get','keywords.add.get','keywords.edit.get');?>
            <li class="nav-item <?php echo (in_array($route_name, $keyword_routes_names)) ? "start active open" : ""?>">
                <a href="{{ route('keywords.list.get') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Keywords</span>
                    <?php echo (in_array($route_name, $keyword_routes_names)) ? "<span class='selected'></span>" : ""?>
                </a>
             </li>
        </ul>
    </div>
</div>