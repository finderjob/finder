<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/*
 * Admin panel routes
 */
Route::group(['prefix'=>'administrator'], function() {
    /*
     * Login, logout routes,
     * */
    Route::get('login','Auth\AdminLoginController@showLoginForm')->name('admin.login.get');
    Route::post('login','Auth\AdminLoginController@login')->name('admin.login.post');
    Route::post('logout','Auth\AdminLoginController@logout')->name('admin.logout.post');

    Route::get('/','Admin\AdminHomeController@index')->name('admin.home');

    Route::post('slug-generate','Admin\AdminHomeController@generateSlug')->name('generate-slug');

    Route::get('users','Admin\AdminHomeController@show')->name('admin.list');
    Route::get('users/add','Admin\AdminHomeController@add')->name('admin.users.add');
    Route::post('users/add','Admin\AdminHomeController@store')->name('admin.users.store');
    Route::post('users/deactivate/{id}','Admin\AdminHomeController@deactivate')->name('admin.users.deactivate');
    Route::post('users/activate/{id}','Admin\AdminHomeController@activate')->name('admin.users.activate');

    Route::get('category','Admin\CatgeoryController@show')->name('category.list.get');
    Route::get('category/add','Admin\CatgeoryController@add')->name('category.add.get');
    Route::post('category/add','Admin\CatgeoryController@store')->name('category.add.post');
    Route::get('category/edit/{id}','Admin\CatgeoryController@edit')->name('category.edit.get');
    Route::post('category/edit/{id}','Admin\CatgeoryController@update')->name('category.edit.post');

    Route::post('category/activate/{id}','Admin\CatgeoryController@activate')->name('category.activate');
    Route::post('category/deactivate/{id}','Admin\CatgeoryController@deactivate')->name('category.deactivate');

    Route::get('sub-category','Admin\SubCatgeoryController@show')->name('sub-category.list.get');
    Route::get('sub-category/add','Admin\SubCatgeoryController@add')->name('sub-category.add.get');
    Route::post('sub-category/add','Admin\SubCatgeoryController@store')->name('sub-category.add.post');
    Route::get('sub-category/edit/{id}','Admin\SubCatgeoryController@edit')->name('sub-category.edit.get');
    Route::post('sub-category/edit/{id}','Admin\SubCatgeoryController@update')->name('sub-category.edit.post');

    Route::post('sub-category/activate/{id}','Admin\SubCatgeoryController@activate')->name('sub-category.activate');
    Route::post('sub-category/deactivate/{id}','Admin\SubCatgeoryController@deactivate')->name('sub-category.deactivate');

    Route::get('keywords','Admin\KeywordsController@show')->name('keywords.list.get');
    Route::get('keywords/add','Admin\KeywordsController@add')->name('keywords.add.get');
    Route::post('keywords/add','Admin\KeywordsController@store')->name('keywords.add.post');
    Route::get('keywords/edit/{id}','Admin\KeywordsController@edit')->name('keywords.edit.get');
    Route::post('keywords/edit/{id}','Admin\KeywordsController@update')->name('keywords.edit.post');

    Route::post('keywords/activate/{id}','Admin\KeywordsController@activate')->name('keywords.activate');
    Route::post('keywords/deactivate/{id}','Admin\KeywordsController@deactivate')->name('keywords.deactivate');

});
