<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllSug extends Model
{
    protected $table = 'all_slug';

    protected $fillable = [
        'slug'
    ];

    public static function checkSlug($slug)
    {
        $availability = self::where('slug','=',$slug)->first();
        if(empty($availability))
        {
           return true;
        }

        return false;
    }

    public static function generateSlug($slug, $i=1)
    {
        $slug = self::slugify($slug);
        $availability = self::checkSlug($slug);
        if($availability)
        {
            return $slug;
        }
        else
        {
            $new_sulg = $slug."-".(int)$i;
            return self::generateSlug($new_sulg, (int)$i+1);
        }
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public static function addSlug($slug)
    {
        $new_slug = new AllSug();
        $new_slug->slug = $slug;
        $new_slug->save();
    }
}
