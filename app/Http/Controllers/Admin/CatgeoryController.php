<?php

namespace App\Http\Controllers\Admin;
use App\AllSug;
use App\Category;
use App\Http\Controllers\Breadcrumbs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CatgeoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Category',''));
        $category_list = Category::all();
        return view('admin.category.list')->with('category_list', $category_list);
    }


    public function add()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Category','category.list.get'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        return view('admin.category.add');
    }

    public function store(Request $request)
    {
        $validator = $this->categoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Category create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $slug = AllSug::generateSlug($request->get('slug'));
        $category = new Category();
        $category->name = $request->get('name');
        $category->slug = $slug;
        $category->created_by = \Auth::guard('admins')->user()->id;
        $category->save();
        AllSug::addSlug($slug);
        session()->flash('success_message','Category created successfully');
        return redirect()->back();
    }

    public function edit($id){

        $category = Category::find($id);
        if(!$category)
        {
            session()->flash('error_message','Category doesn\'t exist');
            return redirect()->to(route('category.list.get'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Category','category.list.get'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));
        return view('admin.category.edit')->with('category', $category);
    }

    public function update($id, Request $request)
    {
        $category = Category::find($id);
        if(!$category)
        {
            session()->flash('error_message','Category doesn\'t exist');
            return redirect()->to(route('category.list.get'));
        }
        $validator = $this->categoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Category update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category_slug = $category->slug;
        if($category_slug !=  $request->get('slug'))
        {
            $slug = AllSug::generateSlug($request->get('slug'));
            if($category_slug != $slug)
            {
                $category->slug = $slug;
                AllSug::addSlug($slug);
            }
        }
        $category->name = $request->get('name');
        $category->save();

        session()->flash('success_message','Category updated successfully');
        return redirect()->back();
    }

    public function categoryFormValidation($request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string'
        ];

        $message = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        return $validator;
    }

    public function deactivate($id)
    {
        $category = Category::find($id);
        if(!$category)
        {
            session()->flash('error_message','Category doesn\'t exist');
            return redirect()->to(route('category.list.get'));
        }
        $category->status = 0;
        $category->save();
        session()->flash('success_message','Category deactivated successfully');
        return redirect()->back();
    }

    public function activate($id)
    {
        $category = Category::find($id);
        if(!$category)
        {
            session()->flash('error_message','Category doesn\'t exist');
            return redirect()->to(route('category.list.get'));
        }
        $category->status = 1;
        $category->save();
        session()->flash('success_message','Category activated successfully');
        return redirect()->back();
    }
}
