<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\AllSug;
use App\Http\Controllers\Breadcrumbs;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Dashboard',''));
        return view('admin.home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Users',''));

        $user_list = Admin::all();
        return view('admin.users.list')->with('user_list',$user_list);
    }

    public function add()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Users','admin.list'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        return view('admin.users.add');
    }

    public function store(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|string|max:255',
            'password' => 'required|string|max:255|min:8',
            'user_type' => 'required|integer'
        ];

        $message = [
            'email.required' => 'The email field is required',
            'email.email' => 'The email should be valid email',
            'email.unique' => 'This email already exists',
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'password.required' =>'The password field is required',
            'password.string' => 'The password should be string',
            'password.max' => 'Maximum characters is 255',
            'password.min' => 'Minimum characters is 8',
            'user_type.required' => 'The user type is required',
            'user_type.integer' => 'User type must be valid'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            session()->flash('error_message','User create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = new Admin();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->user_type = $request->get('user_type');
        $user->save();
        session()->flash('success_message','User created successfully');
        return redirect()->back();
    }

    public function deactivate($id)
    {
        $user = Admin::find($id);
        $logged_user = \Auth::guard('admins')->user()->id;
        if($id == $logged_user)
        {
            session()->flash('error_message','You cannot change your user status');
        }
        else
        {
            if($user)
            {
                $user->status = 0;
                $user->save();
                session()->flash('success_message','User deactivated successfully');
            }
            else
            {
                session()->flash('error_message','This user doesn\'t exists');
            }
        }

        return redirect()->back();
    }

    public function activate($id)
    {
        $user = Admin::find($id);
        $logged_user = \Auth::user()->id;
        if($id == $logged_user)
        {
            session()->flash('error_message','You cannot change your user status');
        }
        else
        {
            if ($user) {
                $user->status = 1;
                $user->save();
                session()->flash('success_message', 'User activated successfully');
            } else {
                session()->flash('error_message', 'This user doesn\'t exists');
            }
        }
        return redirect()->back();
    }

    public function generateSlug(Request $request)
    {
        $keyword = $request->get('keyword');
        if($keyword != '' && !empty($keyword))
        {
            $slug = AllSug::generateSlug($keyword);
            return $slug;
        }

        return '';
    }
}
