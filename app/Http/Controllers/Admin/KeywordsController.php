<?php

namespace App\Http\Controllers\Admin;

use App\AllSug;
use App\Keyword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;
class KeywordsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Keywords',''));
        $keyword_list = Keyword::all();
        return view('admin.keyword.list')->with('keyword_list', $keyword_list);
    }

    public function add()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Keywords','keywords.list.get'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        return view('admin.keyword.add');
    }

    public function store(Request $request)
    {
        $validator = $this->keywordFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Keyword create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $slug = AllSug::generateSlug($request->get('slug'));
        $keyword = new Keyword();
        $keyword->name = $request->get('name');
        $keyword->slug = $slug;
        $keyword->created_by = \Auth::guard('admins')->user()->id;
        $keyword->save();
        AllSug::addSlug($slug);
        session()->flash('success_message','Keyword created successfully');
        return redirect()->back();
    }

    public function edit($id){

        $keyword = Keyword::find($id);
        if(!$keyword)
        {
            session()->flash('error_message','Keyword doesn\'t exist');
            return redirect()->to(route('keywords.list.get'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Keyword','keywords.list.get'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));
        return view('admin.keyword.edit')->with('keyword', $keyword);
    }

    public function update($id, Request $request)
    {
        $keyword = Keyword::find($id);
        if(!$keyword)
        {
            session()->flash('error_message','Keyword doesn\'t exist');
            return redirect()->to(route('keywords.list.get'));
        }
        $validator = $this->keywordFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Keyword update unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $keyword_slug = $keyword->slug;
        if($keyword_slug !=  $request->get('slug'))
        {
            $slug = AllSug::generateSlug($request->get('slug'));
            if($keyword_slug != $slug)
            {
                $keyword->slug = $slug;
                AllSug::addSlug($slug);
            }
        }
        $keyword->name = $request->get('name');
        $keyword->save();

        session()->flash('success_message','Keyword updated successfully');
        return redirect()->back();
    }

    public function keywordFormValidation($request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string'
        ];

        $message = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        return $validator;
    }

    public function deactivate($id)
    {
        $keyword = Keyword::find($id);
        if(!$keyword)
        {
            session()->flash('error_message','Keyword doesn\'t exist');
            return redirect()->to(route('keywords.list.get'));
        }
        $keyword->status = 0;
        $keyword->save();
        session()->flash('success_message','Keyword deactivated successfully');
        return redirect()->back();
    }

    public function activate($id)
    {
        $keyword = Keyword::find($id);
        if(!$keyword)
        {
            session()->flash('error_message','Keyword doesn\'t exist');
            return redirect()->to(route('Keywords.list.get'));
        }
        $keyword->status = 1;
        $keyword->save();
        session()->flash('success_message','Keyword activated successfully');
        return redirect()->back();
    }
}
