<?php

namespace App\Http\Controllers\Admin;

use App\AllSug;
use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Breadcrumbs;

class SubCatgeoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Sub Category',''));
        $sub_category_list = SubCategory::all();
        return view('admin.subcategory.list')->with('sub_category_list', $sub_category_list);
    }

    public function add()
    {
        array_push(Breadcrumbs::$breadcrumb,array('Sub Category','sub-category.list.get'));
        array_push(Breadcrumbs::$breadcrumb,array('Add',''));
        $category = Category::all();
        return view('admin.subcategory.add')->with('category',$category);
    }

    public function store(Request $request)
    {
        $validator = $this->subCategoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Sub category create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $slug = AllSug::generateSlug($request->get('slug'));
        $sub_category = new SubCategory();
        $sub_category->name = $request->get('name');
        $sub_category->slug = $slug;
        $sub_category->category_id = $request->get('category');
        $sub_category->created_by = \Auth::guard('admins')->user()->id;
        $sub_category->save();
        AllSug::addSlug($slug);

        session()->flash('success_message','Sub category created successfully');
        return redirect()->back();
    }

    public function edit($id)
    {
        $sub_category = SubCategory::find($id);
        if(!$sub_category)
        {
            session()->flash('error_message','Sub category doesn\'t exist');
            return redirect()->to(route('sub-category.list.get'));
        }
        array_push(Breadcrumbs::$breadcrumb,array('Sub Category','sub-category.list.get'));
        array_push(Breadcrumbs::$breadcrumb,array('Edit',''));
        $category = Category::all();
        return view('admin.subcategory.edit')->with('category',$category)->with('sub_category',$sub_category);
    }

    public function update($id, Request $request)
    {
        $sub_category = SubCategory::find($id);
        if(!$sub_category)
        {
            session()->flash('error_message','Sub category doesn\'t exist');
            return redirect()->to(route('sub-category.list.get'));
        }
        $validator = $this->subCategoryFormValidation($request);
        if ($validator->fails()) {
            session()->flash('error_message','Sub category create unsuccessfully');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $sub_category_slug = $sub_category->slug;
        if($sub_category_slug !=  $request->get('slug'))
        {
            $slug = AllSug::generateSlug($request->get('slug'));
            if($sub_category_slug != $slug)
            {
                $sub_category->slug = $slug;
                AllSug::addSlug($slug);
            }
        }
        $sub_category->name = $request->get('name');
        $sub_category->category_id = $request->get('category');
        $sub_category->save();

        session()->flash('success_message','Sub category created successfully');
        return redirect()->back();
    }

    public function subCategoryFormValidation($request)
    {

        $categor_ids = $this->categoryList();
        $rules = [
            'name' => 'required|string|max:255',
            'category' => 'required:in'.implode(',', $categor_ids),
            'slug' => 'required|string'
        ];

        $message = [
            'name.required' => 'The name field is required',
            'name.string' => 'The name should be string',
            'name.max' => 'Maximum characters is 255',
            'category.required' => 'The category field is required',
            'category.in' => 'The category doesn\'t exists',
            'slug.required' => 'The slug field is required',
            'slug.string' => 'The slug should be string'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);
        return $validator;
    }

    public function categoryList()
    {
        $category = Category::all();
        $category_ids = array();
        foreach ($category as $cat)
        {
            array_push($category_ids, $cat->id);
        }
        return $category_ids;
    }

    public function deactivate($id)
    {
        $sub_category = SubCategory::find($id);
        if(!$sub_category)
        {
            session()->flash('error_message','Sub Category doesn\'t exist');
            return redirect()->to(route('sub-category.list.get'));
        }
        $sub_category->status = 0;
        $sub_category->save();
        session()->flash('success_message','Sub category deactivated successfully');
        return redirect()->back();
    }

    public function activate($id)
    {
        $sub_category = SubCategory::find($id);
        if(!$sub_category)
        {
            session()->flash('error_message','Sub category doesn\'t exist');
            return redirect()->to(route('sub-category.list.get'));
        }
        $sub_category->status = 1;
        $sub_category->save();
        session()->flash('success_message','Sub category activated successfully');
        return redirect()->back();
    }
}
